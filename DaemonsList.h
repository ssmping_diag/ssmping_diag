#ifndef DAEMONSLIST_H
#define DAEMONSLIST_H
#include <QListWidget>
#include <QListWidgetItem>
#include <QIcon>
#include <QPixmap>
#include "Daemon.h"
class ListItem : public QListWidgetItem
{
    public:
        ListItem(Daemon &d, QListWidget *parent = 0);
        
        int getId();
    private:
        int id;
        QIcon icon;
};
class DaemonsList : public QListWidget
{
    Q_OBJECT
    public:
        DaemonsList(QWidget *parent = 0);

        void addDaemon(Daemon &d);
        void removeDaemon(Daemon &d);
    public slots:
        void updateDaemons();
        void removeSelectedDaemon();
};

#endif
