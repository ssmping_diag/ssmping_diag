#ifndef DAEMON_H
#define DAEMON_H

#include <QString>
#include "Result.h"
#include <QLinkedList>
#include <QColor>
class Daemon
{
    public:
        Daemon();

        int getId();
        QColor getColor();
        void setId(int i);
        QString getAddress();
        void setAddress(QString a);
        void addResult(Result &r);
        void addMcastResult(Result &r);
        void increUCLoss();
        void increMCLoss();
        QLinkedList<Result> getResults();
        QLinkedList<Result> getMcastResults();
        float getAveragePing();
        float getUCPacketLoss();
        float getMCPacketLoss();
    private:
        QString address;
        QColor color;
        QLinkedList<Result> results;
        QLinkedList<Result> mcastResults;
        int id;
        float UCpacketsLost;
        float MCpacketsLost;
        float averagePing;
};

#endif
