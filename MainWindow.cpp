#include "MainWindow.h"

MainWindow::MainWindow():
    QMainWindow()
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    setFixedSize(1100,600);
    tabs = new TabWidget(this);
    setCentralWidget(tabs);
    menubar = new QMenuBar(this);
    menubar->addMenu("File");
    menubar->addMenu("Edit");
    setMenuBar(menubar);
}
