#include "GraphWidget.h"
#include <iostream>
#include <QSizePolicy>
#include <QRect>
#include "Model.h"

GraphWidget::GraphWidget(QWidget *parent):
    QWidget(parent)
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    xAxisOff = 100;
    yAxisOff = 100;
    xRatio = width()/1000;
    yRatio = height()/1000;
    p_max = 0;
    Model &m = Model::getModel();
    QObject::connect(&m, SIGNAL(valueChanged()), this, SLOT(updateData()));
}
void GraphWidget::removeDaemon(int id)
{
}
void GraphWidget::repaintGraph()
{
    p_max = 0;
    lines.clear();
    mcastLines.clear();
    daeIds.clear();

    Model &m = Model::getModel();
    QMap<int, Daemon> daemonMap = m.getDaemons();
    Point p;
    foreach(Daemon d, daemonMap){
        p.x = 0;
        p.y = 0;
        Line l;
        Line l2;
        l.setColor(d.getColor());
        l2.setColor(d.getColor());
        addDaemon(d);
        QLinkedList<Result> res = d.getResults();
        QLinkedList<Result> mcastRes = d.getMcastResults();
        QLinkedList<Result>::iterator mcit = mcastRes.end();
        for (QLinkedList<Result>::iterator ucit = res.begin(); 
                ucit != res.end(); ++ucit){
            if (ucit == res.begin() && res.size()>100)
                ucit = res.end()-100;
            p.x += 8;
            p.y = (*ucit).latency;
            l.addPoint(p);
        }
        p.x = 0;        
        for (QLinkedList<Result>::iterator mcit = mcastRes.begin(); 
                mcit != mcastRes.end(); ++mcit){
            if (mcit == mcastRes.begin() && mcastRes.size()>100)
                mcit = mcastRes.end()-100;
            p.x += 8;
            p.y = (*mcit).latency;
            l2.addPoint(p);
        }
        if (l.getMax() > p_max)
            p_max = l.getMax();
        if (l2.getMax() > p_max)
            p_max = l2.getMax();
               
        lines.push_back(l);
        mcastLines.push_back(l2);
    }
    update(0,0,height(),width());

}
void GraphWidget::setGraphId(int id)
{
    graphId = id;
}

void GraphWidget::paintEvent(QPaintEvent *e)
{
    QPainter painter(this);
    xRatio = width()/1000.0;
    yRatio = height()/1000.0;
    painter.fillRect(QRect(0,0,width(),height()),QColor("white"));
    painter.drawLine(QLineF(xAxisOff*xRatio, yAxisOff*yRatio, 
                        xAxisOff*xRatio, height()-(yAxisOff*yRatio)));
    painter.drawLine(QLineF(xAxisOff*xRatio, height()-yAxisOff*yRatio, 
                        width()-xAxisOff*xRatio, height()-yAxisOff*yRatio));
    for (int i = 0; i < 9; i++){
        painter.drawLine(QLineF(xAxisOff*xRatio, (yAxisOff+i*yAxisOff)*yRatio, 
                            (xAxisOff-10)*xRatio, (yAxisOff+i*yAxisOff)*yRatio));
        painter.setFont(QFont("Helvetica",8));
        painter.drawText((xAxisOff-60)*xRatio, (yAxisOff+i*yAxisOff)*yRatio, 
                            QString::number((int)(p_max-i*p_max/8))+="ms");
        painter.drawLine(QLineF((xAxisOff+xAxisOff*i)*xRatio, height()-yAxisOff*yRatio, 
                            (xAxisOff+xAxisOff*i)*xRatio, height()-(yAxisOff-10)*yRatio));
    }
    painter.setPen(QColor("lightgrey"));
    for (int i = 0; i < 8; i++){
        painter.drawLine(QLineF(xAxisOff*xRatio, (yAxisOff+i*yAxisOff)*yRatio, 
                            (xAxisOff+800)*xRatio, (yAxisOff+i*yAxisOff)*yRatio));
        painter.drawLine(QLineF((xAxisOff+xAxisOff*(i+1))*xRatio, height()-yAxisOff*yRatio, 
                            (xAxisOff+xAxisOff*(i+1))*xRatio, height()-(yAxisOff+800)*yRatio));
    }
    // Start Drawing Lines
    for (int i = 0; i < lines.size(); i++){
        QPen pen;
        pen.setColor(lines[i].getColor());
        pen.setStyle(Qt::SolidLine);
        pen.setWidthF(1.5);
        painter.setPen(pen);
        QQueue<Point> points = lines[i].getPoints();
        for (int j = 0; j < points.size(); j++){
            float maxRatio = (800.0)/p_max;
            if (j == 0)
                continue;
            painter.drawLine(
                QLineF(
                    (yAxisOff+(points[j].x))*xRatio, 

                    height()-(xAxisOff+points[j].y*maxRatio)*yRatio,
                    (yAxisOff+(points[j-1].x))*xRatio, 
                    height()-(xAxisOff+points[j-1].y*maxRatio)*yRatio));
        }
    }
    for (int i = 0; i < mcastLines.size(); i++){
            float maxRatio = (800.0)/p_max;
        QPen pen;
        pen.setColor(mcastLines[i].getColor());
        pen.setStyle(Qt::DotLine);
        pen.setWidthF(1.5);
        painter.setPen(pen);
        QQueue<Point> points = mcastLines[i].getPoints();
        for (int j = 0; j < points.size(); j++){
            if (j == 0)
                continue;
            painter.drawLine(
                QLineF(
                    (yAxisOff+points[j].x)*xRatio, 
                    height()-(xAxisOff+points[j].y*maxRatio)*yRatio,
                    (yAxisOff+points[j-1].x)*xRatio, 
                    height()-(xAxisOff+points[j-1].y*maxRatio)*yRatio));
        }
    }
}

void GraphWidget::addDaemon(Daemon d)
{
    daeIds.push_back(d.getId());
}

void GraphWidget::updateData()
{
    repaintGraph();
}
