#include "StatsWidget.h"
#include "Model.h"

StatsWidget::StatsWidget(QWidget *parent):
    QWidget(parent)
{
    daemonSelected = -1;
    list = new DaemonsList(this);
    layout = new QGridLayout(this);
    rightPanel = new QWidget(this);
    right_layout = new QBoxLayout(QBoxLayout::TopToBottom, rightPanel);
    ucLoss = new QLabel("Unicast Packet Loss: ");
    mcLoss = new QLabel("Multicast Packet Loss: ");
    averagePing = new QLabel("Average Latency: ");
    list->setFixedWidth(400);

    layout->addWidget(list, 0, 0, Qt::AlignLeft);
    layout->addWidget(rightPanel, 0, 1, Qt::AlignLeft);
    setLayout(layout);
    right_layout->addWidget(ucLoss,0,Qt::AlignLeft | Qt::AlignTop);
    right_layout->addWidget(mcLoss,0,Qt::AlignLeft | Qt::AlignTop);
    right_layout->addWidget(averagePing,0,Qt::AlignLeft | Qt::AlignTop);
    right_layout->addSpacing(900);
    rightPanel->setLayout(right_layout);
    
    Model &m = Model::getModel();
    QObject::connect(&m, SIGNAL(daemonsChanged()), this, SLOT(clearDaemon()));
    QObject::connect(&m, SIGNAL(statsChanged(int)), this, SLOT(updateStats(int)));
    QObject::connect(list, SIGNAL(itemClicked(QListWidgetItem *)), 
                        this, SLOT(setSelected(QListWidgetItem *)));
}

void StatsWidget::updateStats(int id)
{
    if (daemonSelected == -1)
        return;
    if (id != daemonSelected)
        return;
    Model &m = Model::getModel();
    ucLoss->setText(QString("Unicast Packet Loss: ") 
                    += QString::number(m.getDaemon(id).getUCPacketLoss())+="%");
    mcLoss->setText(QString("Multicast Packet Loss: ") 
                    += QString::number(m.getDaemon(id).getMCPacketLoss())+="%");
    averagePing->setText(QString("Average Latency: ")
                    += QString::number(m.getDaemon(id).getAveragePing())+="ms");
    update();
}

void StatsWidget::setSelected(QListWidgetItem *item)
{
    ListItem *li = (ListItem *)item;
    daemonSelected = li->getId();
    updateStats(daemonSelected);
}

void StatsWidget::clearDaemon()
{
    daemonSelected = -1;
}
