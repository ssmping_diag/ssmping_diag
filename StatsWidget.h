#ifndef STATSWIDGET_H
#define STATSWIDGET_H

#include <QWidget>
#include <QGridLayout>
#include <QBoxLayout>
#include <QLabel>
#include <QListWidgetItem>
#include "DaemonsList.h"


class StatsWidget: public QWidget
{
    Q_OBJECT
    public:
        StatsWidget(QWidget *parent = 0);

    public slots:
        void updateStats(int id);
        void setSelected(QListWidgetItem *item);
        void clearDaemon();
    private:
        DaemonsList *list;
        QGridLayout *layout;
        QWidget *rightPanel;
        QBoxLayout *right_layout;
        QLabel *ucLoss;
        QLabel *mcLoss;
        QLabel *averagePing;
        int daemonSelected;
};

#endif
