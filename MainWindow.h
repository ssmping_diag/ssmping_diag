#ifndef MAIN_WINDOW
#define MAIN_WINDOW

#include <QMainWindow>
#include <QMenuBar>
#include "TabWidget.h"

class MainWindow : public QMainWindow
{
    public:
        MainWindow();
    private:
        TabWidget *tabs;
        QMenuBar *menubar;
};

#endif

