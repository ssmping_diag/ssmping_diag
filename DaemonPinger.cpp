#include "DaemonPinger.h"
#include "Result.h"
#include "Model.h"
#include <iostream>
#include <sys/socket.h>
#include <sys/types.h>

DaemonPinger::DaemonPinger(int ID):
    id(ID), 
    QThread()
{
    QThread::start();
    Model &m = Model::getModel();
    Daemon d = m.getDaemon(ID);
    address = d.getAddress();
    threadFinished = false;
}
void *DaemonPinger::get_in_addr(sockaddr *sa)
{
    if (sa->sa_family == AF_INET)
        return &(((sockaddr_in*)sa)->sin_addr);
    return &(((sockaddr_in6*)sa)->sin6_addr);
}

void DaemonPinger::setFinished(bool b)
{
    threadFinished = b;
}
void DaemonPinger::run()
{
    int status;
    addrinfo hints;
    addrinfo *servinfo, *mcinfo, *p, *mcp;
    sockaddr_in mcaddr;
    ip_mreq mreq;
    int sockfd, mcsockfd;
    u_int yes = 1;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE;
    addrinfo *myaddr;
    if ((status = getaddrinfo(NULL, "0", &hints, &myaddr)) != 0) {
        fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
        return;
    }
    if ((status = getaddrinfo(address.toStdString().c_str(), "4321", &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
        return;
    }
    sockfd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
    if (sockfd == -1)
        perror("socket");/*
    if (::connect(sockfd, servinfo->ai_addr, servinfo->ai_addrlen))
        perror("connect");*/
    if (setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(yes)) < 0) {
        perror("Reusing ADDR failed");
        return;
    }
    if (bind(sockfd,myaddr->ai_addr, myaddr->ai_addrlen) == -1){
        perror("bind");
        return;
    }
    
    // Create multicast socket.
    // Get mc address info
    if ((status = getaddrinfo("233.3.4.234", "4321", &hints, &mcinfo)) != 0) {
        fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
        exit(1);
    }
    // Create socket
    

    // Construct MC request and set sock option
    mreq.imr_multiaddr.s_addr= inet_addr("233.4.5.234");
    mreq.imr_interface.s_addr = htonl(INADDR_ANY);
    if (setsockopt(sockfd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (const void *)&mreq, sizeof(mreq)) == -1){
        perror("could not set sock opts");
        return;
    }
    // Tester packet
    char msg[] = {81,/**/0,1,0,4,0,0,0,1,/**/0,2,0,4,0,0,0,1,/**/0,3,0,8,0,0,0,0,0,0,0,1,
                    /**/0,4,0,5,1,233,4,5,234};
    unsigned char buff[sizeof(msg)];
    unsigned int seqNum = 1; 
    int fdsmax;
    memset(buff, 0, sizeof(buff));
    fd_set master, readfds;
    FD_ZERO(&readfds);
    FD_ZERO(&master);
    FD_SET(sockfd, &master);
    fdsmax = sockfd;
    Model &model = Model::getModel();

    // Start pinging loop
    while(!threadFinished){
        std::cout << "Pinging...\n";
        double diff,diff2;
        timespec start,end,end2;
        timeval timeout;
        timeout.tv_sec = 2;
        timeout.tv_usec = 0;
        readfds = master;
        bool sock_recv=0, mc_recv=0;
        seqNum++;
        int networkSeq = htonl(seqNum);
        int *p = (int *)(&msg[13]);
        *p = networkSeq;

        // Start send
        clock_gettime(CLOCK_REALTIME, &start);
        if (sendto(sockfd, msg, sizeof(msg), 0, 
                    servinfo->ai_addr, servinfo->ai_addrlen) < sizeof(msg))
            printf("Send failed\n");
        for (int i = 0; i < 2; i++){
            // Start receiving packets
            FD_ZERO(&readfds);
            readfds = master;
            if (select(fdsmax+1, &readfds, NULL, NULL, &timeout) == 0){
                printf("timeout expired\n");
                if (!sock_recv){
                    model.addPacketLost(id, 0);
                    end = start;
                } else if (!mc_recv) {
                    model.addPacketLost(id, 1);
                    end2 = start;
                }
                break;
            } else printf("select exit\n");
            if (FD_ISSET(sockfd, &readfds)){
                if (sock_recv == 0) {
                    printf("Unicast waiting...\n");
                    socklen_t addrlength = servinfo->ai_addrlen;
                    if (recvfrom(sockfd, buff, sizeof(buff), 0,
                            servinfo->ai_addr, &addrlength) == -1){
                        perror("Error receiving");
                    } else {
                        int seqTemp = ntohl(*(int *)(&buff[13]));
                        if (seqTemp < seqNum){
                            printf("Timed out packet recvd\n");
                            i--;
                        } else {
                            clock_gettime(CLOCK_REALTIME, &end);
                            sock_recv = 1;
                        }
                    }
                } else {
                    printf("Multicast waiting...\n");
                    socklen_t addrlength = servinfo->ai_addrlen;
                    if (recvfrom(sockfd, buff, sizeof(buff), 0,
                            servinfo->ai_addr, &addrlength) == -1){
                        perror("Error receiving");
                    } else {
                        int seqTemp = ntohl(*(int *)(&buff[13]));
                        if (seqTemp != seqNum){
                            printf("Timed out packet recvd\n");
                            i--;
                        } else {
                            clock_gettime(CLOCK_REALTIME, &end2);
                            mc_recv = 1;
                        }
                    }
                }
            }
        }
        diff = ((end.tv_sec + end.tv_nsec/1000000000.0)
                -(start.tv_sec + start.tv_nsec/1000000000.0));
        diff2 = ((end2.tv_sec + end2.tv_nsec/1000000000.0)
                 -(start.tv_sec + start.tv_nsec/1000000000.0));
        for (int i = 0; i < sizeof(buff); i++)
            std::cout << (int)buff[i];
        printf("\n");
        Result r;
        r.timestamp = 100;
        r.latency = diff*1000.0;

        model.addResult(id,r);
        r.latency = diff2*1000.0;
        model.addMcastResult(id,r);
        usleep(250000);
    }
    printf("Thread ended\n");
}
void DaemonPinger::setId(int ID)
{
    id = ID;
}

int DaemonPinger::getId()
{
    return id;
}

