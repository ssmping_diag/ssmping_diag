#ifndef CENTRALWIDGET_H
#define CENTRALWIDGET_H

#include <QWidget>
#include <QPaintEvent>
#include <QGridLayout>
#include "GraphWidget.h"
#include "ControlsWidget.h"

class CentralWidget: public QWidget
{
    public:
            CentralWidget(QWidget *parent = 0);

        void paintEvent(QPaintEvent *pe);
    private:
        QGridLayout *layout;
        GraphWidget *graph;
        ControlsWidget *controls;
};

#endif
