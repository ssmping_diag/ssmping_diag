#ifndef CONTROLSWIDGET_H
#define CONTROLSWIDGET_H

#include <QWidget>
#include <QLineEdit>
#include <QListView>
#include <QPushButton>
#include <QGridLayout>
#include "DaemonsList.h"
class LineEdit : public QLineEdit
{
    Q_OBJECT

    public: 
        LineEdit(QWidget *parent = 0);

    public slots:
        void addDaemon();
};

class ControlsWidget : public QWidget
{
    public:
        ControlsWidget(QWidget *parent = 0);
    private:
        LineEdit *daemonAdd;
        DaemonsList *daemonList;
        QPushButton *addButton;
        QPushButton *removeButton;

        QGridLayout *layout;
};

#endif
