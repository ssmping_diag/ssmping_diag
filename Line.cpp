#include "Line.h"

Line::Line():
    p_max(0)
{
}

QQueue<Point> Line::getPoints()
{
    return points;
}

void Line::addPoint(Point &p)
{
    if (p.y > p_max)
        p_max = p.y;
    points.enqueue(p);
}

int Line::getId()
{
    return id;
}
int Line::getMax()
{
    return p_max;
}
QColor Line::getColor()
{
    return color;
}
void Line::setId(int i)
{
    id = i;
}

void Line::setColor(QColor c)
{
    color = c;
}
