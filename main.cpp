#include "MainWindow.h"
#include "DaemonPinger.h"
#include <QApplication>
#include "Daemon.h"
#include "Model.h"
#include "Controller.h"
int main(int argc, char *argv[])
{
    QApplication app(argc,argv);
    
    MainWindow window;
    window.setWindowTitle("SSM/ASM Diagnostics Tool");
    window.show();
    Controller *c = new Controller();
    Model &m = Model::getModel();
    //DaemonPinger dp2("ssmping.ecs.soton.ac.uk",1);
    //DaemonPinger dp3("ssmping.erg.abdn.ac.uk",2);
    app.exec();
}
