#include "DaemonsList.h"
#include "Model.h"
#include <stdio.h>
DaemonsList::DaemonsList(QWidget *parent):
    QListWidget(parent)
{
    //setViewMode(QListWidget::IconMode);
    setIconSize(QSize(10,10));
    Model &m = Model::getModel();
    QObject::connect(&m,SIGNAL(daemonsChanged()),this,SLOT(updateDaemons()));
    updateDaemons();
}

void DaemonsList::addDaemon(Daemon &d)
{
    ListItem *li = new ListItem(d,this);
    addItem(li);
}

void DaemonsList::removeDaemon(Daemon &d)
{

}

void DaemonsList::removeSelectedDaemon()
{
    Model &m = Model::getModel();

    QList<QListWidgetItem *> items = selectedItems();
    foreach (QListWidgetItem *i, items){
        ListItem *temp = (ListItem*)i;
        m.removeDaemon(temp->getId());
        updateDaemons();
    }
}

void DaemonsList::updateDaemons()
{
    clear();
    Model &m = Model::getModel();
    QMap<int,Daemon> daemons = m.getDaemons();
    foreach (Daemon d, daemons){
        addDaemon(d);
    }
}

ListItem::ListItem(Daemon &d, QListWidget *parent):
    QListWidgetItem(parent)
{
    QPixmap pm(10,10);
    pm.fill(d.getColor());
    icon.addPixmap(pm);
    setIcon(icon);
    id = d.getId();
    QString text;
    QString id(d.getId());
    QString addr(d.getAddress());
    text = "";
    text += QString::number(d.getId());
    text += ' ';
    text += d.getAddress();
    setText(text);
}

int ListItem::getId()
{
    return id;
}
