#ifndef LINE_H
#define LINE_H

#include <QQueue>
#include <QColor>
#include "Point.h"

class Line
{
    public:
        Line();
        QQueue<Point> getPoints();
        void addPoint(Point &p);
        int getId();
        int getMax();
        QColor getColor();
        void setId(int i);
        void setColor(QColor c);
    private:
        int  id;
        int p_max;
        QColor color;
        QQueue<Point> points;
};

#endif
