#include "Daemon.h"
#include <stdlib.h>
Daemon::Daemon():
    color(QColor(rand()%200+25,rand()%200+25,rand()%200+25)),
    UCpacketsLost(0),
    MCpacketsLost(0)
{
}

int Daemon::getId()
{
    return id;
}

QColor Daemon::getColor()
{
    return color;
}

void Daemon::setId(int i)
{
    id = i;
}

QString Daemon::getAddress()
{
    return address;
}

void Daemon::setAddress(QString a)
{
    address = a;
}

void Daemon::addResult(Result &r)
{
    results.push_back(r);
    averagePing = ((averagePing*(results.size()-1))+r.latency)/results.size();
}

void Daemon::addMcastResult(Result &r)
{
    mcastResults.push_back(r);
}

void Daemon::increUCLoss()
{
    UCpacketsLost++;
}

void Daemon::increMCLoss()
{
    MCpacketsLost++;
}

QLinkedList<Result> Daemon::getResults()
{
    return results;
}

QLinkedList<Result> Daemon::getMcastResults()
{
    return mcastResults;    
}
float Daemon::getAveragePing()
{
    return averagePing;
}
float Daemon::getUCPacketLoss()
{
    if (results.size()+UCpacketsLost == 0)
        return -1;
    return (1-results.size()/(results.size()+UCpacketsLost))*100;
}
float Daemon::getMCPacketLoss()
{
    if (mcastResults.size()+MCpacketsLost == 0)
        return -1;
    return (1-mcastResults.size()/(mcastResults.size()+MCpacketsLost))*100;
}


