#ifndef TABWIDGET_H
#define TABWIDGET_H

#include <QWidget>
#include <QTabWidget>
#include "CentralWidget.h"
#include "StatsWidget.h"

class TabWidget: public QTabWidget
{
    public:
        TabWidget(QWidget *parent = 0);

    private: 
        CentralWidget *cw;
        StatsWidget *sw;
        QWidget *test;
};

#endif
