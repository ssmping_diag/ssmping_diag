#include "ControlsWidget.h"
#include "Model.h"
LineEdit::LineEdit(QWidget *parent):
    QLineEdit(parent)
{
    setPlaceholderText("Enter address of daemon...");
}

void LineEdit::addDaemon()
{
    Model &m = Model::getModel();
    Daemon d;
    d.setAddress(text());
    d.setId(m.getMaxId()+1);
    m.addDaemon(d.getId(), d);
}

ControlsWidget::ControlsWidget(QWidget *parent):
    QWidget(parent)
{
    daemonAdd = new LineEdit(this);
    daemonList = new DaemonsList(this);
    addButton = new QPushButton("Add Daemon", this);
    removeButton = new QPushButton("Remove Daemon", this);
    layout = new QGridLayout(this);

    QObject::connect(addButton, SIGNAL(clicked()), daemonAdd, SLOT(addDaemon()));
    QObject::connect(removeButton, SIGNAL(clicked()), daemonList, SLOT(removeSelectedDaemon()));
//    daemonAdd->setPlaceholderText("Enter address of daemon");
    layout->addWidget(daemonAdd,0,1);
    layout->addWidget(addButton,0,0);
    layout->addWidget(daemonList,1,1);
    layout->addWidget(removeButton,1,0);
    layout->setAlignment(removeButton, Qt::AlignTop);

    setLayout(layout);
}
