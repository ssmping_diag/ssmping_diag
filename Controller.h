#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include <QMap>
#include "DaemonPinger.h"
#include "Daemon.h"
class Controller : public QObject
{
    Q_OBJECT
    public:
        Controller();
        ~Controller();
    public slots:
        void addDaemon(Daemon d);
        void removeDaemon(int id);
    private:
        QMap<int, DaemonPinger *> daemons;
};

#endif
