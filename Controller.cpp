#include "Model.h"
#include "Daemon.h"
#include "Controller.h"

Controller::Controller()
{
    Model &m = Model::getModel();
    QObject::connect(&m, SIGNAL(daemonsAdded(Daemon)), this, SLOT(addDaemon(Daemon)));
    QObject::connect(&m, SIGNAL(daemonRemoved(int)), this, SLOT(removeDaemon(int)), Qt::DirectConnection);
}

void Controller::addDaemon(Daemon d)
{
    DaemonPinger *dp = new DaemonPinger(d.getId());
    daemons.insert(d.getId(), dp);
}

void Controller::removeDaemon(int id)
{
    daemons[id]->setFinished(true);
    daemons[id]->wait();
    delete daemons[id];
    daemons.remove(id);
}

Controller::~Controller()
{
    foreach (DaemonPinger *dp, daemons){
        delete dp;
    }
}
