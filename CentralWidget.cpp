#include "CentralWidget.h"
#include <QPainter>
#include <QSizePolicy>
CentralWidget::CentralWidget(QWidget *parent):
    QWidget(parent)
{
    layout = new QGridLayout;
    graph = new GraphWidget(this);
    controls = new ControlsWidget(this);

    setLayout(layout);
  //  ->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
  
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    layout->addWidget(controls,0,0);
    layout->addWidget(graph,0,1);
}

void CentralWidget::paintEvent(QPaintEvent *pe)
{
    QPainter painter(this);
    //painter.setBackground(brush);
}
