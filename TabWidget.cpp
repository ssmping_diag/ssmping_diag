#include "TabWidget.h"

TabWidget::TabWidget(QWidget *parent):
    QTabWidget(parent)
{
    cw = new CentralWidget(this);
    sw = new StatsWidget(this);

    addTab(cw, "Latency");
    addTab(sw, "Statistics");
}


