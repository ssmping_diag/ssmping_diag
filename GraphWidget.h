#ifndef GRAPHWIDGET_H
#define GRAPHWIDGET_H

#include <QWidget>
#include <QPainter>
#include <QPaintEvent>
#include <QImage>
#include "Line.h"
#include "Daemon.h"

class GraphWidget: public QWidget
{
    Q_OBJECT
    public:
        GraphWidget(QWidget *parent = 0);
        void repaintGraph();
        void setGraphId(int id);
        void paintEvent(QPaintEvent *e);
    public slots:
        void updateData();
        void addDaemon(Daemon d);
        void removeDaemon(int id);
    private:
        int graphId;
        float xAxisOff;
        float yAxisOff;
        float xRatio;
        float yRatio;
        float p_max;
        QImage image;
        QList<Line> lines;
        QList<Line> mcastLines;
        QList<int> daeIds;

};

#endif
