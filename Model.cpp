#include "Model.h"
#include <stdio.h>
#include <iostream>
Model *Model::model = 0;

Model::Model()
{
    maxId = 0;
}

Model &Model::getModel()
{
    if (model == 0)
        model = new Model();
    return *model;
}
/*
Daemon Model::getDaemon(int id)
{
    daemonMutex.lock();
    if
    Daemon d = daemons[id];
    daemonMutex.unlock();
    return d;
}
*/
QMap<int,Daemon> Model::getDaemons()
{
    daemonMutex.lock();
    QMap<int,Daemon> temp = daemons;
    daemonMutex.unlock();
    return temp;
}
void Model::addPacketLost(int id, int mc_or_uc)
{
    daemonMutex.lock();
    if (daemons.contains(id)){
        if (mc_or_uc)
            daemons[id].increMCLoss();
        else daemons[id].increUCLoss();
        daemonMutex.unlock();
        emit statsChanged(id);
        return;
    } else daemonMutex.unlock();
}
Graph &Model::getGraph(int id)
{
    return graphs[id];
}

void Model::addGraph(int id, Graph &g)
{
    
}

void Model::addDaemon(int id, Daemon &d)
{
    daemonMutex.lock();
    if (id > maxId)
        maxId = id;
    daemons.insert(id,d);
    daemonMutex.unlock();
    emit daemonsChanged();
    emit daemonsAdded(d);
}

void Model::addResult(int id, Result &r)
{
    daemonMutex.lock();
    if (daemons.contains(id)){
        daemons[id].addResult(r);
        daemonMutex.unlock();
        emit statsChanged(id);
        return;
    } else daemonMutex.unlock();
}

void Model::addMcastResult(int id, Result &r)
{
    daemonMutex.lock();
    if (daemons.contains(id)){
        daemons[id].addMcastResult(r);
        daemonMutex.unlock();
        emit valueChanged();
        emit statsChanged(id);
        return;
    } else daemonMutex.unlock();
}

void Model::removeDaemon(int id)
{
    daemonMutex.lock();
    if (daemons.contains(id))
        daemons.remove(id);
    daemonMutex.unlock();
    emit daemonRemoved(id);
}

int Model::getMaxId()
{
    daemonMutex.lock();
    int tid = maxId;
    daemonMutex.unlock();
    return tid;
}
