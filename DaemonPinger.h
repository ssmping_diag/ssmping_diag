#ifndef DAEMONPINGER_H
#define DAEMONPINGER_H

#include <string>
#include <QThread>
#include <QSemaphore>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
#include <arpa/inet.h>
#include <stdio.h>
class DaemonPinger: public QThread
{   
    public:
            DaemonPinger(int ID);

        void run();
        void setId(int id);
        int getId();
        void setFinished(bool b);
        void *get_in_addr(sockaddr *sa);
    private:
        int id;
        QString address; 
        bool threadFinished;

};


#endif
