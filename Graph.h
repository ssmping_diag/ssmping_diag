#ifndef GRAPH_H
#define GRAPH_H

#include <QObject>
#include <QVector>
#include <QList>
#include "Line.h"
#include "Result.h"

class Graph
{
    public:
        int getId();
        void addDaemon(int id);
        QList<int> getDaemonIds();
    private:
        QList<int> daemonIds;

        int id;
};

#endif
