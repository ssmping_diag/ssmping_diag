#include "Graph.h"

void Graph::addDaemon(int id)
{
    daemonIds.push_back(id);
}

int Graph::getId()
{
    return id;
}
QList<int> Graph::getDaemonIds()
{
    return daemonIds;
}
