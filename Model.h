#ifndef MODEL_H
#define MODEL_H

#include <QMap>
#include <QObject>
#include <QMutex>
#include "Daemon.h"
#include "Graph.h"
#include "Result.h"
class Model: public QObject
{
    Q_OBJECT
    public:
        static Model &getModel();
//        Daemon getDaemon(int id);
        QMap<int,Daemon> getDaemons();
        Graph &getGraph(int id);
        void addGraph(int id, Graph &g);
        void addDaemon(int id, Daemon &d);
        void addResult(int id, Result &r);
        void addMcastResult(int id, Result &r);
        void addPacketLost(int id, int mc_or_uc);
        void removeDaemon(int id);
        int getMaxId();
    signals:
        void valueChanged();
        void daemonsChanged();
        void daemonsAdded(Daemon d);
        void daemonRemoved(int id);
        void statsChanged(int id);
    private:
            Model();
        static Model *model;
        QVector<Graph> graphs;
        QMap<int,Daemon> daemons;
        QMutex daemonMutex;
        int maxId;
};

#endif
